<?php

namespace IKNSA\AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use IKNSA\BlogBundle\Entity\Comment;



class LoadComment extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $comment1 = new Commment;
        $comment1->setComment('Comment 1');
        $comment1->setCreatedAt(new \Datetime);
        $comment1->setUser($this->getReference('commentator'));
        $comment1->setPost($this->getReference('post-one'));
        $this->addReference('comment-one', $comment1);

        $manager->persist($comment1);


        $comment2 = new Commment;
        $comment2->setComment('Comment 2');
        $comment2->setCreatedAt(new \Datetime);
        $comment2->setUser($this->getReference('commentator'));
        $comment2->setPost($this->getReference('post-two'));
        $this->addReference('comment-two', $comment2);

        $manager->persist($comment2);


        $comment3 = new Commment;
        $comment3->setComment('Comment 3');
        $comment3->setCreatedAt(new \Datetime);
        $comment3->setUser($this->getReference('commentator'));
        $comment3->setPost($this->getReference('post-three'));
        $this->addReference('comment-three', $comment3);

        $manager->persist($comment3);


        $manager->flush();


    }

    public function getOrder()
    {
        return 3;
    }




}
