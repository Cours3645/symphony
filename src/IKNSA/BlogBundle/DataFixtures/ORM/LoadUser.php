<?php

namespace IKNSA\AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;



class LoadUser extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    private $container;

    public function load(ObjectManager $manager)
    {
        $userManager = $this->getContainer->get('fos_user.user_manager');


        $user1 = $userManager->createUser();
        $user1->setUsername('user');
        $user1->setEmail('user@iknsa.com');
        $user1->setPlainPassword('1234');
        $user1->setEnabled(true);
        $user1->setLastLogin(new \Datetime('NOW'));
        $user1->setRoles(array('ROLE_USER'));
        $this->addReference('user', $user1);

        $manager->persist($user1);


        $user2 = $userManager->createUser();
        $user2->setUsername('admin');
        $user2->setEmail('admin@iknsa.com');
        $user2->setPlainPassword('1234');
        $user2->setEnabled(true);
        $user2->setLastLogin(new \Datetime('NOW'));
        $user2->setRoles(array('ROLE_ADMIN'));
        $this->addReference('admin', $user2);

        $manager->persist($user2);


        $user3 = $userManager->createUser();
        $user3->setUsername('contributor');
        $user3->setEmail('contributor@iknsa.com');
        $user3->setPlainPassword('1234');
        $user3->setEnabled(true);
        $user3->setLastLogin(new \Datetime('NOW'));
        $user3->setRoles(array('ROLE_CONTRIBUTOR'));
        $this->addReference('contributor', $user3);

        $manager->persist($user3);


        $user4 = $userManager->createUser();
        $user4->setUsername('commentator');
        $user4->setEmail('commentator@iknsa.com');
        $user4->setPlainPassword('1234');
        $user4->setEnabled(true);
        $user4->setLastLogin(new \Datetime('NOW'));
        $user4->setRoles(array('ROLE_COMMENTATOR'));
        $this->addReference('commentator', $user4);

        $manager->persist($user4);

        $manager->flush();


    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->getContainer = $container;
    }

    public function getOrder()
    {
        return 1;
    }




}
