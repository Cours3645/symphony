<?php

namespace IKNSA\AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use IKNSA\BlogBundle\Entity\Post;



class LoadPost extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $post1 = new Post;
        $post1->setTitle('Titre 1');
        $post1->setSummary('Summary 1');
        $post1->setContent('Content 1');
        $post1->setCreatedAt(new \Datetime);
        $post1->setUser($this->getReference('contributor'));
        $post1->setExtension('jpeg');
        $this->addReference('post-one', $post1);

        $manager->persist($post1);


        $post2 = new Post;
        $post2->setTitle('Titre 2');
        $post2->setSummary('Summary 2');
        $post2->setContent('Content 2');
        $post2->setCreatedAt(new \Datetime);
        $post2->setUser($this->getReference('contributor'));
        $post2->setExtension('jpeg');
        $this->addReference('post-two', $post2);

        $manager->persist($post2);


        $post3 = new Post;
        $post3->setTitle('Titre 3');
        $post3->setSummary('Summary 3');
        $post3->setContent('Content 3');
        $post3->setCreatedAt(new \Datetime);
        $post3->setUser($this->getReference('contributor'));
        $post3->setExtension('jpeg');
        $this->addReference('post-three', $post3);

        $manager->persist($post3);


        $manager->flush();


    }

    public function getOrder()
    {
        return 2;
    }




}
