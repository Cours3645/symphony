<?php

namespace IKNSA\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array('label' => 'Titre ', 'attr' => array('class' => 'form-control')))
            ->add('summary', TextType::class, array('label' => 'Summary ', 'attr' => array('class' => 'form-control')))
            ->add('content', TextareaType::class, array('label' => 'Content ', 'attr' => array('class' => 'form-control')))
            ->add('file', FileType::class, array('label' => 'Image ', 'attr' => array('class' => 'form-control'))) ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IKNSA\BlogBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'iknsa_blogbundle_post';
    }


}
