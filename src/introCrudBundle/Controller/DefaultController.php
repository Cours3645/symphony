<?php

namespace introCrudBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('introCrudBundle:Blog:index.html.twig');
    }
}
